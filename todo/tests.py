from django.test import TestCase, Client
from .models import todoPost
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse, resolve
from .views import PostUpdateView, PostDeleteView


class testTodoModel(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='morti' , email='belanela@yahoo.com', password='Himori1243')
        self.user.save()
        self.post = todoPost(title='test', content='test project', start_time='now', finish_time='1 hour', author=self.user)
        self.post.save()
        self.client = Client()
        self.client.login(username = 'morti', password = 'Himori1243')


    def test_home_page(self):
        response = self.client.get('/')
        self.assertContains(response, "test")
        self.assertContains(response, "start at: now")
        self.assertContains(response, "test project")

    def test_post_page(self):
        response = self.client.get('/todo/1/')
        self.assertContains(response, "test")
        self.assertContains(response, "start at: now")
        self.assertContains(response, "test project")
        self.assertContains(response, "update")
        self.assertContains(response, "delete")

    
    def test_update_page(self):
        response = self.client.post(reverse('todo-update', args=('1')), {'title': 'test123', 'content': 'testing', 'start_time': '1', 'finish_time': '2'})
        self.post.refresh_from_db()
        self.assertEqual(self.post.title, 'test123')
        self.assertEqual(self.post.start_time, '1')


    def test_loging_required_for_update(self):
        self.client.logout()
        response = self.client.get('/todo/1/update')
        self.assertRedirects(response, '/login/?next=/todo/1/update')

    
    def test_update_view(self):
        view = resolve('/todo/1/update')
        self.assertEqual(view.func.view_class, PostUpdateView)


    def test_csrf(self):
        response = self.client.get('/todo/1/update')
        self.assertContains(response, 'csrfmiddlewaretoken')


    def test_update_form_input(self):
        response = self.client.get('/todo/1/update')
        self.assertContains(response, '<input')

    
    def test_delete_page(self):
        response = self.client.post(reverse('todo-delete', args=('1')))
        self.assertEqual(response.status_code, 302)
        response2 = self.client.get('/')
        self.assertContains(response2, "you don't have any todo yet")


    def test_delete_view(self):
        view = resolve('/todo/1/delete')
        self.assertEqual(view.func.view_class, PostDeleteView)


    def test_csrf(self):
        response = self.client.get('/todo/1/delete')
        self.assertContains(response, 'csrfmiddlewaretoken')