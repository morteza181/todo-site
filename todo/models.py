from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse


# models define the variable of our site. in this example 
# todo posts is our models in which we want a user can add them
# after adding post we must type dj makemigrations, and then dj migrate
class todoPost(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    start_time = models.CharField(max_length=20)
    finish_time = models.CharField(max_length=20)
    submit_date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    # after creating post by postcreateview, the django want to redirect in some place. 
    # this function redirect to todo detail page.
    def get_absolute_url(self):
        return reverse('todo-detail', kwargs={'pk': self.pk})

