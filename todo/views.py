from django.shortcuts import render, HttpResponse
# this django view make simple way for displaying items.
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from . import models
from django.contrib import messages


def home(request):
    context = {
        'todos': models.todoPost.objects.filter(author_id=request.user.id)
    }
    return render(request, 'todo/home.html', context)


class PostListView(ListView):
    pass
    # model = models.todoPost
    # # this code define the template name. this code can placed in url path as before. 
    # template_name='todo/home.html'
    # # this name is one of the reserved name on django.
    # context_object_name = 'todos'
    # # ordering is reserved name and this code display the newest post earlier.
    # ordering = ['-submit_date']


class PostDetailView(UserPassesTestMixin, DetailView):
    model = models.todoPost
    template_name='todo/todo-detail.html'

    def test_func(self):
        todo = self.get_object()
        if self.request.user == todo.author:
            return True
        return False


class PostCreateView(LoginRequiredMixin ,CreateView):
    model = models.todoPost
    template_name='todo/todo-create.html'
    # the fields specify what kind of content must be change.
    fields = ['title', 'content', 'start_time', 'finish_time']

    # each post need author, this function specify the corrent user as author
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = models.todoPost
    # the template of the update and create todo can be same
    template_name='todo/todo-update.html'
    # the fields specify what kind of content must be change.
    fields = ['title', 'content', 'start_time', 'finish_time']

    # each post need author, this function specify the corrent user as author
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


    # this line is come after userPassesTestMixin and make the user create todo can change that.
    def test_func(self):
        todo = self.get_object()
        if self.request.user == todo.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = models.todoPost
    template_name='todo/todo-delete.html'
    # success url define when the todo deleted successfully, redirect user to home page
    success_url = '/'

    def test_func(self):
        todo = self.get_object()
        if self.request.user == todo.author:
            return True
        return False 


