from django.urls import path
from .views import (
    PostListView, 
    PostDetailView, 
    PostCreateView,
    PostUpdateView,
    PostDeleteView,
)
from . import views

urlpatterns = [
    path('', views.home, name='home-page'),    #this change with below code.
    # path('', PostListView.as_view(), name='home-page'),    
    path('todo/<int:pk>/', PostDetailView.as_view(), name='todo-detail'),    
    path('todo/new/', PostCreateView.as_view(), name='todo-create'),
    path('todo/<int:pk>/update', PostUpdateView.as_view(), name='todo-update'),    
    path('todo/<int:pk>/delete', PostDeleteView.as_view(), name='todo-delete'),    
]