from django.test import TestCase, Client
from django.contrib.auth.models import User
from .forms import UserUpdateForm, UserRegisterForm
from django.contrib.auth.forms import UserCreationForm

        

class userTest(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='morti' , email='belanela@yahoo.com', password='Himori1243')
        user.save()
        

    def test_unsigned_user_home_page(self):
        response = Client().get('/')
        self.assertContains(response, 'this site is for manage your todo list')


    def test_valid_user_register(self):
        form = UserRegisterForm(data={'username':'asger' , 'email':'hoseini@yahoo.com', 'password1':'Himori123', 'password2': 'Himori123'})
        response = Client().get('/', follow=True)
        self.assertTrue(form.is_valid())
        self.assertEqual(response.status_code, 200)



    def test_invalid_user_register(self):
        form = UserRegisterForm(data={'username': 'morti2', 'email': 'morti@yahoo'})
        self.assertFalse(form.is_valid())

    
    def test_user_is_exist(self):
        form = UserRegisterForm(data={'username':'morti' , 'email':'belanela@yahoo.com', 'password1':'Himori1243', 'password2': 'Himori1243'})
        self.assertFalse(form.is_valid())

    
    def test_user_home_page(self):
        self.client = Client()
        self.client.login(username='morti', password= 'Himori1243')
        response = self.client.get('/')
        self.assertContains(response, "you don't have any todo yet")
        self.client.logout()

    
    def test_user_profile_page(self):
        self.client = Client()
        self.client.login(username='morti', password= 'Himori1243')
        response = self.client.get('/profile/')
        self.assertContains(response, 'morti')
        self.assertContains(response, 'belanela@yahoo.com')
        self.client.logout()


    def test_user_update_profile(self):
        self.client = Client()
        self.client.login(username='morti', password= 'Himori1243')
        form = UserUpdateForm(data={'username': 'abay', 'email': 'abay@yahoo.com'})
        self.assertTrue(form.is_valid())
        form.save()
        response = self.client.get('/profile/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.client.logout()

